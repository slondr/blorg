
#+setupfile: /home/eric/org/blog/org-template/style.org
#+TITLE: Eric S. Londres's Blorg

- [[file:history-page.org][Auto-generating a History page for a static personal website]]
- [[file:creating-a-blorg-in-2020.org][Creating a Blog in Org-Mode in 2020]]
